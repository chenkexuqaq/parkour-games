﻿
using Cinemachine;
using System.Collections;
using UnityEngine;

namespace Assets.Scirpts
{
    public class PlayerCamera : MonoBehaviour
    {
        private CinemachineVirtualCamera _virtualCamera;

        private void Awake()
        {
            _virtualCamera = GameObject.FindObjectOfType<CinemachineVirtualCamera>();
        }
        private void Start()
        {
            _virtualCamera.Follow = this.transform;
            _virtualCamera.LookAt = this.transform;
        }

    }
}