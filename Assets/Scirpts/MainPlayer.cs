using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UIElements.UxmlAttributeDescription;

public class MainPlayer : MonoBehaviour
{
    #region Unity方法
    private void Awake()
    {
        InitPhysicsComponent();
        InitAnimComponent();
    }
    private void Start()
    {
        InitPos();
        InitMovement();
        InitJump();
    }
    private void Update()
    {
        UpdateCaught();
    }

    private void OnDrawGizmos()
    {
        DrawWallSonser();
        DrawGroundSensor();
        DrawFontLine();
        DrawUMagnetRanged();
    }


    
    #endregion

    #region 物理
    private Rigidbody _rb;
    public void InitPhysicsComponent()
    {
        _rb = GetComponent<Rigidbody>();
    }
    public void SetVelocityY(float yVelocity)
    {
        _rb.velocity = new Vector3(_rb.velocity.x, yVelocity, _rb.velocity.z);
    }
    private void SetVelocityZero()
    {
        _rb.velocity = Vector3.zero;
    }
    #endregion

    #region 动画
    private Animator _animator;
    public void InitAnimComponent()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    private void PlayAnim(string animName)
    {
        _animator.Play(animName);
    }
    #endregion

    #region 移动
    [SerializeField] private float _moveSpeed;
    public float MoveSpeed => _moveSpeed;
    private float _orginSpeed;
    private void InitMovement() 
    {
        _orginSpeed = _moveSpeed;
    }
    
    #endregion

    #region 跳
    [Header("跳")]

    [SerializeField] private float _jumPower;
    [SerializeField] private float _jumpDetectInterval;

    private float _currentJumpPower;
    private void InitJump() 
    {
        _currentJumpPower = _jumPower;
    }
    public bool CanJump()
    {
        return OnGround && !_sideJumping&&!_caught&&!_stunned;
    }
    public void Jump()
    {
        if (CanJump())
        {
            PlayAnim(AnimName.Jump);
            SetVelocityY(_currentJumpPower);
            StartCoroutine(InJump());
        }
    }

    private IEnumerator InJump()
    {
        yield return new WaitForSeconds(_jumpDetectInterval);
        while (true)
        {
            if (OnGround)
            {
                Debug.Log("EnterGround");
                EnterGround();
                break;
            }
            yield return null;
        }
    }


    private void EnterGround()
    {
        _animator.Play(AnimName.Run);
        SetVelocityZero();
    }


    #endregion

    #region Roll
    [Header("roll")]
    [SerializeField] private float _rollingTime;
    [SerializeField] private float _rollingSpeed;
    private bool _rolling;
    
    public bool CanRoll() 
    {
        return !_rolling&&!_sideJumping&!_caught&&!_stunned;
    }
    public void Roll() 
    {
        if (CanRoll()) 
        {
            _moveSpeed = _rollingSpeed;
            PlayAnim(AnimName.Roll);
            _rolling = true;
            StartCoroutine(RollCoro());
        }
    }

    private IEnumerator RollCoro()
    {
        yield return new WaitForSeconds(_rollingTime);
        _rolling = false;
        _moveSpeed = _orginSpeed;
        PlayAnim(AnimName.Run);
    }

    #endregion

    #region 下坠

    #endregion

    #region 跳跃侧跳
    #endregion

    #region 侧跳
    [Header("侧跳")]
    [SerializeField] private float _jumpPower;
    [SerializeField]private float _sideJumpDistance;
    [SerializeField] private float _sideJumpDuration;
    private bool _sideJumping;
    public void SideJump(bool isRight) 
    {
        if (CanSideJump())
        {
            Vector3 jumpTarget = Vector3.zero;
            if (isRight) 
            {
                if (HasWallOnRight) 
                {
                    Stun(true);
                }
                else 
                {
                    jumpTarget = TransformFunction.AddPosX(transform, _sideJumpDistance);
                    DoSideJump(jumpTarget);
                }
            }
            else 
            {
                if (HasWallOnLeft) 
                {
                    Stun(false);
                }
                else 
                {
                    jumpTarget = TransformFunction.SubPosX(transform, _sideJumpDistance);
                    DoSideJump(jumpTarget);
                   
                }
            }
    
        }

    }

    private void DoSideJump(Vector3 jumpTarget)
    {
        _sideJumping = true;
        PlayAnim(AnimName.Jump);
        _rb.DOJump(jumpTarget, _jumpPower, 1, _sideJumpDuration).OnComplete(SideJumpEnd);
    }

    private bool CanSideJump()
    {
        return !_sideJumping&&!_rolling&&!_caught&&!_stunned;
    }

    private void SideJumpEnd()
    {
        PlayAnim(AnimName.Run);
        _sideJumping=false;
    }
    #endregion

    #region 昏眩
    private Vector3 _orignPos;
    private bool _stunned=false;
    private void InitPos() 
    {
        _orignPos = transform.position;
    }
    private void Stun(bool isRight)
    {
        
        _stunned = true;

        StartStunAnim(isRight);

        PlayAnim(AnimName.Stun);
        Debug.Log("昏眩");
        
        
    }
    [Header("昏眩")]
    [SerializeField] private float _stunDistance=0.5f;
    [SerializeField] private float _stunDuration=1f;
    private void StartStunAnim(bool isRight)
    {
        Vector3 targetMove=Vector3.zero;
        _orignPos = transform.position;
        if (isRight) 
        {
            targetMove=TransformFunction.AddPosX(transform, _stunDistance);
        }
        else 
        {
            targetMove=TransformFunction.SubPosX(transform, _stunDistance);
        }

        Sequence sequence = DOTween.Sequence();

        sequence.AppendCallback(() => {
            Camera.main.DOShakePosition(1, new Vector3(3, 3, 0));
            PlayAnim(AnimName.Stun);
        });

        sequence.Append(transform.DOMove(targetMove, _stunDuration));
        sequence.Append(transform.DOMove(_orignPos,_stunDuration));
        sequence.AppendCallback(() => {
            PlayAnim(AnimName.Run);
            _stunned=false;
        });
       
        sequence.Play();

    }


    #endregion

    #region 被抓
    private void UpdateCaught()
    {
        if (HasObstacleAhead)
        {
            GetCaught();
            return;
        }
    }
    public Action<MainPlayer> OnDieEvent;
    private bool _caught = false;
    public void GetCaught() 
    {
        if (CanCaught()) 
        {
            _caught = true;
            PlayAnim(AnimName.Hit);
            OnDieEvent?.Invoke(this);
            GameManager.Instance.UpdateGameState(GameState.End);
            UIManager.Instance.ShowBootUI(false);
            UIManager.Instance.ShowUMagnetUI(false);
            Destroy(this.gameObject);
        }
    }

    private bool CanCaught()
    {
        return !_rolling&&!_sideJumping;
    }
    #endregion

    #region 感知器
    [SerializeField] private Transform _wallSensor;
    [SerializeField] private LayerMask _wallMask;
    [SerializeField] private float _distance;
    public bool HasWallOnRight => Physics.Linecast(_wallSensor.position, new Vector3(_wallSensor.position.x + _distance, _wallSensor.position.y, _wallSensor.position.z), _wallMask);
    public bool HasWallOnLeft => Physics.Linecast(_wallSensor.position, new Vector3(_wallSensor.position.x - _distance, _wallSensor.position.y, _wallSensor.position.z), _wallMask);
    public bool HasWallOnSide => HasWallOnLeft && HasWallOnRight;

    private void DrawWallSonser()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_wallSensor.position, new Vector3(_wallSensor.position.x + _distance, _wallSensor.position.y, _wallSensor.position.z));
        Gizmos.DrawLine(_wallSensor.position, new Vector3(_wallSensor.position.x - _distance, _wallSensor.position.y, _wallSensor.position.z));
    }

    [SerializeField] private Transform _groundSensor;
    [SerializeField] private LayerMask _groundMask;
    [SerializeField] private float _groundRadius;
    public bool OnGround => Physics.OverlapSphere(_groundSensor.position, _groundRadius, _groundMask).Length>0;

    private void DrawGroundSensor() 
    {
        Gizmos.DrawWireSphere(_groundSensor.position, _groundRadius);
    }

    [SerializeField] private Transform _fontSensor;
    [SerializeField] private LayerMask _fontMask;
    //[SerializeField] private Vector3 _size;
    [SerializeField] private float _fontDistance;

    public bool HasObstacleAhead => Physics.Linecast(_fontSensor.position,TransformFunction.AddPosZ(_fontSensor,_fontDistance),_fontMask);

    private void DrawFontLine() 
    {
        Gizmos.color = Color.yellow;
        //Gizmos.DrawWireCube(_fontSensor.position, _size);
        Gizmos.DrawLine(_fontSensor.position, TransformFunction.AddPosZ(_fontSensor, _fontDistance));
    }

    #endregion

    #region 金币系统 
    public Action<int> OnGoldChanged;
    private int _gold=0;
    public int Gold 
    {
        get {return _gold; }
        set 
        {
            if (value <= 0) 
            {
                value = 0;
            }
            if (value != _gold) 
            {
                _gold = value;
                OnGoldChanged?.Invoke(_gold);
            }
        }
    }

    #endregion

    #region 靴子道具
    [Header("靴子道具")]
    [SerializeField] private float _bootJumpPower;
  
    [SerializeField] private float _hasBootDuration = 10f;
    public void GetBoot()
    {
        _currentJumpPower = _bootJumpPower;
        
        var bootUI= UIManager.Instance.ShowBootUI(true);
        bootUI.Driver(_hasBootDuration);
        StartCoroutine(StartBootExpendCoro());
    }

    private IEnumerator StartBootExpendCoro()
    {
        yield return new WaitForSeconds(_hasBootDuration);
        _currentJumpPower = _jumPower;
        var bootUI=UIManager.Instance.ShowBootUI(false);
        bootUI.Driver(_hasBootDuration);
    }

    #endregion

    #region U形磁体道具
    [Header("U形磁体道具")]
    [SerializeField] private Transform _uMagnetTransform;
    [SerializeField] private float _uMagnetRadius=2;
    [SerializeField] private LayerMask _goldMask;
    [SerializeField] private float _uMagnetDuration=10;

    public float UMagnetDuration => _uMagnetDuration;

    private bool _hasUMagnet;
    
    private void DrawUMagnetRanged() 
    {
        Gizmos.DrawWireSphere(_uMagnetTransform.position, _uMagnetRadius);
    }
    public void GetUMagnet()
    {
        _hasUMagnet = true;
        UMagnetUI uMagnetUI=UIManager.Instance.ShowUMagnetUI(true);
        uMagnetUI.Driver(_uMagnetDuration);
        StartCoroutine(StartUMagnetExpendCoro());
        StartCoroutine(ExpendEnd());
    }
    private IEnumerator ExpendEnd()
    {
        yield return new WaitForSeconds(_uMagnetDuration);
        _hasUMagnet = false;
        UIManager.Instance.ShowUMagnetUI(false);
    }


    private IEnumerator StartUMagnetExpendCoro()
    {
        while (true) 
        {
            Collider[] colliders = Physics.OverlapSphere(_uMagnetTransform.position, _uMagnetRadius, _goldMask);
            foreach (var collider in colliders)
            {
                var gold=collider.GetComponent<Gold>();
                gold.Absorb(this);
            }
            if (!_hasUMagnet) 
            {
                yield break;
            }
            yield return null;
        }
    }
    #endregion

}
