﻿using System.Collections;
using UnityEngine;

public class Boot : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var player=other.GetComponent<MainPlayer>();
        if (player) 
        {
            player.GetBoot();
            Destroy(gameObject);
        }
    }
}
