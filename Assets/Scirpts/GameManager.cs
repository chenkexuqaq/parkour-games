﻿using System;
using System.Collections;
using UnityEngine;


public enum GameState 
{
    None,
    Start,
    Running,

    End,
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private void Awake()
    {
        if(Instance == null) 
        {
            Instance = this;
        }
    }

    private void Start()
    {
        UpdateGameState(GameState.Start);
    }

    private GameState _currentState=GameState.None;

    public Action<GameState> OnStateChanged;
    public void UpdateGameState(GameState state) 
    {
        if(state == _currentState) 
        {
            return;
        }
        switch (state)
        {
            case GameState.Start:
                StartGame();
                _currentState = GameState.Start;
                break;
            case GameState.Running:
                GamePlay();
                _currentState = GameState.Running;
                break;
            case GameState.End:
                GameEnd();
                _currentState = GameState.End;
                break;
            default:
                break;
        }
        OnStateChanged?.Invoke(_currentState);
    }
    private void StartGame()
    {
    }
    private void GamePlay()
    {
        LevelManager.Instance.InitLevel();
        ScoreSystem.Instance.ResetScore();
    }
   
    private void GameEnd()
    {
       
    }

}
