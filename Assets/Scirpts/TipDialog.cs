﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TipDialog : MonoBehaviour
{
    [SerializeField] private Button _cancelButton;
    [SerializeField] private Button _okButton;

    private void OnEnable()
    {
        _cancelButton.onClick.AddListener(CloseTipDialog);
        _okButton.onClick.AddListener(returnMenu);
       
    }
    private void OnDisable()
    {
        _cancelButton.onClick.RemoveListener(CloseTipDialog);
        _okButton.onClick.RemoveListener(returnMenu);
    }
    private void CloseTipDialog()
    {
        this.gameObject.SetActive(false);
    }
    private void returnMenu()
    {
       
    }


}
