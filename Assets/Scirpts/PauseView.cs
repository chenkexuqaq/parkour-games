﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PauseView : MonoBehaviour
{
    [SerializeField] private Button _pauseButton;
    [SerializeField] private GameObject _pauseUI;
  
    private void OnEnable()
    {
        _pauseButton.onClick.AddListener(OnPauseClick);
    }
    private void OnDisable()
    {
        _pauseButton.onClick.RemoveListener(OnPauseClick);
    }

    private void OnPauseClick()
    {
        Time.timeScale = 0;
        ScoreSystem.Instance.StopCalculate();
        _pauseUI.SetActive(true);
    }
}
