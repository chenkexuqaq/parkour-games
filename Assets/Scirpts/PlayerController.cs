﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    #region Unity方法
    private void Awake()
    {
        InitPlayer();
    }

    private void Start()
    {
        
    }
    private void Update()
    {
        
    }
    #endregion
    #region Input
    public void JumpInput(InputAction.CallbackContext ctx) 
    {
        if (ctx.phase == InputActionPhase.Performed) 
        {
            Jump();
        }
    }

  
    public void RollInput(InputAction.CallbackContext ctx) 
    {
        if (ctx.phase == InputActionPhase.Performed)
        {
            Roll();
        }
    }

    
    public void LeftInput(InputAction.CallbackContext ctx)
    {
        if (ctx.phase == InputActionPhase.Performed)
        {
            _isRight = false;
            SideJump();
        }
    }
    public void RightInput(InputAction.CallbackContext ctx) 
    {
        if (ctx.phase == InputActionPhase.Performed)
        {
            _isRight = true;
            SideJump();
        }
    }
    public void Touch(InputAction.CallbackContext ctx) 
    {
        
        var vector2=ctx.ReadValue<Vector2>();
        Debug.Log(ctx.phase);
        Debug.Log(vector2);
    }

    private MainPlayer _player;
    private void InitPlayer()
    {
        _player = GetComponent<MainPlayer>();
    }
    #endregion

    private void Jump()
    {
        _player.Jump();
    }
    private void Roll()
    {
        _player.Roll();
    }
    private bool _isRight;
    private void SideJump() 
    {
        _player.SideJump(_isRight);
    }
}
