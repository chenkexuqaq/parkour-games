﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;


public class Gold : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed;
    private void Update()
    {
        transform.Rotate(0,_rotateSpeed*Time.deltaTime,0);
    }

    private void OnTriggerEnter(Collider other)
    {
        MainPlayer player=other.GetComponent<MainPlayer>();
        if (player) 
        {
            player.Gold++;
            Destroy(this.gameObject);
        }
    }

    [SerializeField] private float _absordSpeed=0.3f;
    private bool _absord=false;
    public void Absorb(MainPlayer player)
    {
        if (!_absord) 
        {
            _absord=true;
            transform.DOMove(player.transform.position, _absordSpeed).OnComplete(() => { Destroy(this.gameObject); });
        }
    }
}
