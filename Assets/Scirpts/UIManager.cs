﻿using System;
using System.Collections;
using UnityEngine;


public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    private void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;
        }
    }

    private void OnEnable()
    {
        GameManager.Instance.OnStateChanged += OnUIChanged;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= OnUIChanged;
    }

    [SerializeField] private GameObject _startUI;
    [SerializeField] private GameObject _gameUI;
    [SerializeField] private GameObject _endUI;
    private void OnUIChanged(GameState state)
    {
        if (_currentUI != null) 
        {
            _currentUI.SetActive(false);
        }

        switch (state)
        {
            case GameState.Start:
                _currentUI = _startUI;
                break;
            case GameState.Running:
                _currentUI = _gameUI;
                break;
            case GameState.End:
                _currentUI = _endUI;
                break;
            default:
                break;
        }
        _currentUI.SetActive(true);
    }

    private GameObject _currentUI;

   
    [SerializeField] private BootUI _bootUI;
    public BootUI ShowBootUI(bool active) 
    {
        _bootUI.gameObject.SetActive(active);
        return _bootUI;
    }

    [SerializeField] private UMagnetUI _uMagnetUI;
    public UMagnetUI ShowUMagnetUI(bool active)
    {
        _uMagnetUI.gameObject.SetActive(active);
        return _uMagnetUI;
    }
}
