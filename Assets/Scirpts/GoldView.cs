﻿using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GoldView : MonoBehaviour
{
    [SerializeField] private Text _goldText;
    private MainPlayer _player;
    void Awake()
    {
        _player = GameObject.FindObjectOfType<MainPlayer>();
    }
    private void OnEnable()
    {
        _player.OnGoldChanged += OnGoldChanged;
    }

    private void OnDisable()
    {
        _player.OnGoldChanged -= OnGoldChanged;
    }

    private void OnGoldChanged(int value)
    {
        _goldText.text = value.ToString();
    }
}
