﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreView : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _multiplyingPowerText;
    private void OnEnable()
    {
        ScoreSystem.Instance.OnScoreChanged += OnScoreChanged;
        ScoreSystem.Instance.OnMultiplyingPowerChanged += OnMultiplyingPowerChanged;
    }


    private void OnDisable()
    {
        ScoreSystem.Instance.OnScoreChanged -= OnScoreChanged;
        ScoreSystem.Instance.OnMultiplyingPowerChanged -= OnMultiplyingPowerChanged;
    }
    private void OnScoreChanged(int value)
    {
        _scoreText.text = value.ToString();
    }
    private void OnMultiplyingPowerChanged(int value)
    {
        _multiplyingPowerText.text = value.ToString();
    }

}