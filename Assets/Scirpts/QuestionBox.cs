﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuestionBox : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed = 10;
    private void Update()
    {
        transform.Rotate(0, _rotateSpeed * Time.deltaTime, 0);
    }

    [SerializeField] private List<GameObject> _propList;
    private void OnTriggerEnter(Collider other)
    {
        var player=other.GetComponent<MainPlayer>();
        if (player) 
        {
            int num = Random.Range(0, _propList.Count-1);
            GameObject.Instantiate(_propList[num],transform.position,Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}