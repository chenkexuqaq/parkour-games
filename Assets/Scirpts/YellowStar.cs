﻿using System.Collections;
using UnityEngine;

public class YellowStar : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed=10;
    private void Update()
    {
        transform.Rotate(0, _rotateSpeed * Time.deltaTime, 0);
    }


    [SerializeField] private int _power;
    private void OnTriggerEnter(Collider other)
    {
        var player=other.GetComponent<MainPlayer>();

        if (player) 
        {
            ScoreSystem.Instance.MultiplyingPower += _power; 
            Destroy(this.gameObject);
        }
    }

}
