﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventInfo 
{
    
}
public class EventInfo : IEventInfo 
{
    public object Sender;
    public Action EventAction;
}
public class EventInfo<F> : IEventInfo
{
    public object Sender;
    public Action<F> EventAction;
}
public class EventInfo<F,S> : IEventInfo
{
    public object Sender;
    public Action<F,S> EventAction;
}

public class EventCenter
{
    private static Dictionary<string, IEventInfo> _events = new Dictionary<string, IEventInfo>();

    public static void AddEventListener(string name, Action eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo).EventAction += eventAction;
        }
        else
        {
            _events.Add(name, new EventInfo() { EventAction = eventAction });
        }
    }

    public static void AddEventListener<F>(string name, Action<F> eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F>).EventAction += eventAction;
        }
        else
        {
            _events.Add(name, new EventInfo<F>() { EventAction = eventAction });
        }
    }

    public static void AddEventListener<F,S>(string name, Action<F,S> eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F,S>).EventAction += eventAction;
        }
        else
        {
            _events.Add(name, new EventInfo<F,S>() { EventAction = eventAction });
        }
    }
    public static void RemoveEventListener(string name, Action eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo).EventAction -= eventAction;
        }
    }
    public static void RemoveEventListener<F>(string name, Action<F> eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F>).EventAction -= eventAction;
        }
    }
    public static void RemoveEventListener<F, S>(string name, Action<F, S> eventAction)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F, S>).EventAction -= eventAction;
        }
    }

    public static void TriggerEvent(string name)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo).EventAction?.Invoke();
        }
    }
    public static void TriggerEvent<F>(string name, F arg)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F>).EventAction?.Invoke(arg);
        }
    }
    public static void TriggerEvent<F, S>(string name, F fArg, S sArg)
    {
        if (_events.ContainsKey(name))
        {
            (_events[name] as EventInfo<F, S>).EventAction?.Invoke(fArg, sArg);
        }
    }
}
