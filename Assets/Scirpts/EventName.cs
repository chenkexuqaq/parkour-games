﻿using System.Collections;
using UnityEngine;


public static class EventName
{
    public const string LEVEL_TRIGGER = "level_trigger";
    public const string GET_BOOT = "get_boot";
    public const string GET_STAR = "get_star";
    public const string GET_CATPAW = "get_car_paw";
    public const string GET_QUESTION_BOX = "get_question_box";
    public const string GET_U_MAGNET = "get_u_magnet";


}
