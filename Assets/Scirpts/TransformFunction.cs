﻿using System.Collections;
using UnityEngine;


public static class TransformFunction 
{
    public static Vector3 AddPosX(Transform self,float x) 
    {
        return new Vector3(self.position.x+x,self.position.y,self.position.z);
    }

    public static Vector3 SubPosX(Transform self, float x)
    {
        return new Vector3(self.position.x - x, self.position.y, self.position.z);
    }

    public static Vector3 AddPosY(Transform self, float y)
    {
        return new Vector3(self.position.x, self.position.y+y, self.position.z);
    }

    public static Vector3 SubPosY(Transform self, float y)
    {
        return new Vector3(self.position.x, self.position.y - y, self.position.z);
    }

    public static Vector3 AddPosZ(Transform self, float z)
    {
        return new Vector3(self.position.x, self.position.y, self.position.z+z);
    }
    public static Vector3 SubPosZ(Transform self, float z)
    {
        return new Vector3(self.position.x, self.position.y, self.position.z-z);
    }
}
