﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    [SerializeField] private Button _returnGameButton;
    [SerializeField] private Button _returnMenuButton;
    [SerializeField] private GameObject _tipDialog;
  
    private void OnEnable()
    {
        _returnGameButton.onClick.AddListener(ClosePauseUI);
        _returnMenuButton.onClick.AddListener(OpenTipDialog);
    }

    private void OnDisable()
    {
        _returnGameButton.onClick.RemoveListener(ClosePauseUI);
        _returnMenuButton.onClick.RemoveListener(OpenTipDialog);
    }
    private void ClosePauseUI()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1.0f;
        ScoreSystem.Instance.StartCalculate();
    }
    private void OpenTipDialog()
    {
        _tipDialog.SetActive(true);
    }
}
