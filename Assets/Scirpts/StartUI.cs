﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StartUI : MonoBehaviour
{
    [SerializeField] private Button _startButton;

    private void OnEnable()
    {
        _startButton.onClick.AddListener(StartButtonClicked);
    }

    private void OnDisable()
    {
        _startButton.onClick.RemoveListener(StartButtonClicked);
    }

    private void StartButtonClicked()
    {
        GameManager.Instance.UpdateGameState(GameState.Running);
    }
}
