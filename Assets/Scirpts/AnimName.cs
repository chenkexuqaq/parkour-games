﻿
using System.Linq.Expressions;

public class AnimName
{
    public const string Run = "run";
    public const string Jump = "jump";
    public const string Stun = "stun";
    public const string Roll = "roll";
    public const string Dying = "dying";
    public const string Hit = "hit";
}
