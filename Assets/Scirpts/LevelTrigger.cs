﻿using System.Collections;
using UnityEngine;


public class LevelTrigger : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        var player=other.GetComponent<MainPlayer>();
        if (player) 
        {
            EventCenter.TriggerEvent(EventName.LEVEL_TRIGGER);
            Destroy(this.gameObject);
        }
    }
}
