﻿using System;
using System.Collections;
using UnityEngine;


public class BootUI : MonoBehaviour
{
    
    public void Driver(float bootDuration)
    {
        StartCoroutine(ExpendCoro(bootDuration));
    }

    private IEnumerator ExpendCoro(float bootDuration)
    {
        for (int i =Mathf.CeilToInt(bootDuration); i >0; i--)
        {
            UpdateItem(i);
            yield return new WaitForSeconds(1);
        }
    }

    [SerializeField] private Transform _itemParent;
    [SerializeField] private GameObject _item;
    private void UpdateItem(int count)
    {
        for (int i = 0; i < _itemParent.childCount; i++)
        {
            Destroy(_itemParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < count; i++)
        {
            GameObject.Instantiate(_item,_itemParent);
        }
    }
}
