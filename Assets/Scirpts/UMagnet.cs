﻿using System;
using System.Collections;
using UnityEngine;


public class UMagnet : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed=10;
    private void Update()
    {
        transform.Rotate(0, _rotateSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player=other.GetComponent<MainPlayer>();
        if (player != null) 
        {
            player.GetUMagnet();
            Destroy(this.gameObject);
        }
    }
  
}
