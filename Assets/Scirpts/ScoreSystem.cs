﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;


public class ScoreSystem : MonoBehaviour
{
    public static ScoreSystem Instance;
    private int _currentScore = 0;
    public Action<int> OnScoreChanged;

    public int CurrentScore 
    {
        get 
        {
            
            return _currentScore; 
        }
        set 
        {
            if (value != _currentScore) 
            {
                _currentScore = value;
                OnScoreChanged?.Invoke(value);
            }
        }
    }


    [SerializeField] private int _multiplyingPower=5;
    public Action<int> OnMultiplyingPowerChanged;
    public int MultiplyingPower 
    {
        get { return _multiplyingPower; }
        set 
        {
            if (value != _multiplyingPower) 
            {
                _multiplyingPower = value;
                OnMultiplyingPowerChanged?.Invoke(value);
            }
            
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        StartCalculate();
    }
    public void StartCalculate() 
    {
        StartCoroutine(CalcaulateCoro());
    }
    public void StopCalculate() 
    {
        StopAllCoroutines();
    }


    private IEnumerator CalcaulateCoro()
    {
        while (true) 
        {
            CurrentScore += _multiplyingPower;
            yield return null;
        }
    }
    public void ResetScore()
    {
        CurrentScore = 0;
    }
}