﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameEndView : MonoBehaviour
{
    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _returnStartMenu;
    private void OnEnable()
    {
        _continueButton.onClick.AddListener(OnContinueButtonClicked);
        _returnStartMenu.onClick.AddListener(OnReturnStartButtonClicked);
    }
    private void OnDisable()
    {
        _continueButton.onClick.RemoveListener(OnContinueButtonClicked);
        _returnStartMenu.onClick.RemoveListener(OnReturnStartButtonClicked);
    }


    private void OnContinueButtonClicked()
    {
        GameManager.Instance.UpdateGameState(GameState.Running);
    }
    private void OnReturnStartButtonClicked()
    {
        GameManager.Instance.UpdateGameState(GameState.Start);
    }
    

}
