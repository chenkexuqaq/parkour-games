using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{

    public int Index;
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        UpdateGroundMove();
    }

 
    private void OnDisable()
    {
        _player.OnDieEvent -= OnPlayerDeath;
    }
    private void OnPlayerDeath(MainPlayer player)
    {
        SetGroundRunning(true);
    }
    private MainPlayer _player;
    private void Init()
    {
        _player =GameObject.FindObjectOfType<MainPlayer>();
        _player.OnDieEvent += OnPlayerDeath;
    }
    private bool stopping;
    private void UpdateGroundMove()
    {
        if (!stopping) 
        {
            transform.Translate(0,0,-_player.MoveSpeed*Time.deltaTime);
        }
    }

    public void SetGroundRunning(bool done) 
    {
        if (done) 
        {
            stopping = true;
        }else
        {
            stopping=false;
        }
    }

}
